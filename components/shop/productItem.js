import React from 'react';
import {View , Text , StyleSheet, Image, Button, TouchableNativeFeedback, TouchableOpacity, Platform} from 'react-native';
import colors from '../../constants/colors';


const ProductItem = props =>{

    let TouchableCmp = TouchableOpacity;

    if(Platform.OS === "android" && Platform.version >= 21){
        TouchableCmp = TouchableNativeFeedback;
    }

    return ( 
           <View style={styles.screen}> 
             <View style={styles.imageContainer}>
             <TouchableCmp onPress={props.onViewDetails}>
             <Image source={{uri:props.image}} style={styles.image}/>
             </TouchableCmp>
             </View>
             
             <View style={styles.details}>
             <Text>{props.title}</Text>
             <Text>${props.price.toFixed(2)}</Text>
             </View>
             <View style={styles.actions}>
                 <Button color={colors.primary} title="View Details" onPress={props.onViewDetails}/>
                 <Button color={colors.primary} title="Add To Car" onPress={props.onAddToCar}/>
             </View>
            </View>
            )
};

const styles= StyleSheet.create({
    screen:{
        shadowColor:'black',
        shadowOpacity:0.2,
        shadowOffset:{width:0,height:2},
        shadowRadius:8,
        elevation:5,
        borderRadius:10,
        backgroundColor:'white',
        height:300,
        margin:20
    },
    title:{
      fontSize:18
    },
    price:{
       color:'#888',
       fontSize:14
    },
    details:{
       alignItems:'center',
       padding:10,
       height:'15%'
    },
    actions:{
        flexDirection:'row',
        justifyContent:'space-between',
        padding:10,
        height:'25%'
    },
    image:{
        width:'100%',
        height:'100%'
    },
    imageContainer:{
        width:'100%',
        height:'60%',
        borderTopLeftRadius:10,
        borderTopRightRadius:10, 
        overflow:'hidden'
    }
});

export default ProductItem;