import React from 'react';
import {FlatList, View,Text, StyleSheet} from 'react-native';
import { TouchableNativeFeedback, TouchableOpacity } from 'react-native-gesture-handler';
import {useSelector} from 'react-redux';
import ProductItem from '../../components/shop/productItem';
const ProductOverviewScreen = props =>{

    const products = useSelector(state => state.products.availableProducts);
    // const viewDetailsHandler =(props)=>{
    //     props.navigation.navigate('')
    //     console.log('view Detatils clicked!!')
    // }


    const addToCarHandler =()=>{
        console.log('Add To Cart clicked!!')
    }

return <FlatList data={products} keyExtractor={item=> item.id} renderItem={itemData=> <ProductItem image={itemData.item.imageUrl} 
  title={itemData.item.title}  price={itemData.item.price}   onViewDetails={()=>{
      props.navigation.navigate('productDetails',{
          productId:itemData.item.id,
          productTitle:itemData.item.title
      });
  }} onAddToCar={addToCarHandler}/>}/>
}

ProductOverviewScreen.navigationOptions= navData => {
    return {headerTitle: "All Products"};
    
}

const styles = StyleSheet.create({
    screen:{
        textAlign:"center",
        fontSize:18
    }
});

export default ProductOverviewScreen;
