import React from 'react';
import {createStackNavigator} from 'react-navigation-stack';
import {createAppContainer} from 'react-navigation';
import ProductOverviewScreen from '../screens/shop/ProductOverviewScreen';
import colors  from '../constants/colors';
import ProductDetailScreen from '../screens/shop/ProductDetailScreen';




const ProductsNavigator = createStackNavigator({
    productsOverview: ProductOverviewScreen,
    productDetails: ProductDetailScreen
     },{
    defaultNavigationOptions:{
       
        headerStyle: {
            backgroundColor: Platform.OS === "android" ? colors.primary : "white",
          },
          headerTintColor: Platform.OS === "android" ? "white" : colors.primary,
    }
});

export default createAppContainer(ProductsNavigator);

